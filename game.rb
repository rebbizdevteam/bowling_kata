require "rspec"

class Game


  def initialize
    @score = 0
    @rolls = []
  end

  def roll(pins)
    @rolls << pins

    if last_frame_was_spare
      @score += pins * 2
    else
      @score += pins
    end

    if @rolls.length.odd? && pins == 10
      @rolls << nil
    end

    if last_frame_was_strike
      @score += pins
    end
  end

  def score
    @score
  end

  def last_frame_was_strike
    if @rolls.length.odd?
      @rolls[-2] == 10
    end
  end

  def last_frame_was_spare
    sum_of_last_frame == 10
  end

  def sum_of_last_frame
    if @rolls.length > 2 && @rolls.length.odd?
      @rolls[-2].to_i + @rolls[-3].to_i 
    end
  end
end

describe Game do
  let(:game) { Game.new }
  it "should be be able to produce an instance" do
    expect(Game.new).to be_an_instance_of Game
  end

  it "scores zero for all gutter rolls" do
    20.times do #frame
      game.roll(0)
    end
    expect(game.score).to eq(0)
  end

  it "scores 1s for all frames" do
    10.times do #frame
      game.roll(1)
    end

    expect(game.score).to eq(10)
  end

  it "scores spare frame with the next roll" do
    [4, 6, 3, 0].each do |i|
      game.roll i
    end
    expect(game.score).to eq(16)
  end

  it "scores spare frame if spare frame is second frame" do
    [1, 1, 2, 8, 5].each { |i| game.roll i }

    expect(game.score).to eq(22)
  end

  it "scores spares if spare was scored twice" do
    [4, 6, 3, 7, 6].each { |i| game.roll i }
    expect(game.score).to eq(35)
  end

  it "scores spares if spare was in the mid frame" do
    [4, 6, 1, 0, 4, 6, 1].each { |i| game.roll i}
    expect(game.score).to eq(24)
  end

  it "scores strike with next frame scores" do
    [10, 3, 3].each { |i| game.roll i} 
    expect(game.score).to eq 22
  end
  # 10, 3, 3 - 22
end